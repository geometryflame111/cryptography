import javax.crypto.Cipher;
import java.security.*;
import java.util.Base64;

class AsymmetricCryptography {
    private PublicKey publicKey;
    private PrivateKey privateKey;

    public AsymmetricCryptography() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);
        KeyPair pair = keyGen.generateKeyPair();
        this.publicKey = pair.getPublic();
        this.privateKey = pair.getPrivate();
    }

    public PublicKey getPublicKey() {
        return this.publicKey;
    }

    public PrivateKey getPrivateKey() {
        return this.privateKey;
    }

    public String encryptText(String msg, PublicKey key) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return Base64.getEncoder().encodeToString(cipher.doFinal(msg.getBytes()));
    }

    public String decryptText(String msg, PrivateKey key) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, key);
        return new String(cipher.doFinal(Base64.getDecoder().decode(msg)));
    }

    public static void main(String[] args) {
        try {
            AsymmetricCryptography ac = new AsymmetricCryptography();
            String originalMessage = "Yes, you heard me right. I did it!";
            PublicKey pubKey = ac.getPublicKey();
            PrivateKey privateKey = ac.getPrivateKey();

            String encryptedMessage = ac.encryptText(originalMessage, pubKey);
            String decryptedMessage = ac.decryptText(encryptedMessage, privateKey);

            System.out.println("Original Message: " + originalMessage);
            System.out.println("Encrypted Message: " + encryptedMessage);
            System.out.println("Decrypted Message: " + decryptedMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
